package com.rentalcarsapi.controllers;

import com.rentalcarsapi.controllers.DTO.UserReservationRequest;
import com.rentalcarsapi.entities.Car;
import com.rentalcarsapi.entities.Reservation;
import com.rentalcarsapi.entities.User;
import com.rentalcarsapi.repos.CarRepository;
import com.rentalcarsapi.repos.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;

@RestController
@RequestMapping("/rentalcars/users")
@CrossOrigin
public class UserController {

    private UserRepository userRepository;
    private CarRepository carRepository;

    public UserController(UserRepository userRepository, CarRepository carRepository) {
        this.userRepository = userRepository;
        this.carRepository = carRepository;
    }

    @PostMapping
    public ResponseEntity<User> save(@Valid @RequestBody UserReservationRequest userReservationRequest) throws URISyntaxException{
        Reservation reservation = new Reservation();
        reservation.setPickupDate(new Timestamp(userReservationRequest.getPickupDate().getTime()));
        reservation.setReturnDate(new Timestamp(userReservationRequest.getReturnDate().getTime()));
        Car car = carRepository.findById(userReservationRequest.getCarId()).get();

        reservation.setTotalPrice(reservation.calculateTotalPrice(car.getPricePerHour()));
        reservation.setCar(car);
        carRepository.save(car);

        User user = new User();
        user.setFirstName(userReservationRequest.getUserFirstName());
        user.setMiddleName(userReservationRequest.getUserMiddleName());
        user.setLastName(userReservationRequest.getUserLastName());
        user.setEmail(userReservationRequest.getUserEmail());
        user.setPhoneNumber(userReservationRequest.getUserPhoneNumber());
        user.addReservation(reservation);
        userRepository.save(user);

        return ResponseEntity.created(new URI("/rentalcars/reservations" + reservation.getId())).body(user);
    }

}
