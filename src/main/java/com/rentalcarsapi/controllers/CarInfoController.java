package com.rentalcarsapi.controllers;


import com.rentalcarsapi.entities.CarInfo;
import com.rentalcarsapi.repos.CarInfoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;


@RestController
@RequestMapping("/carsInfo")
@CrossOrigin
public class CarInfoController {

    private CarInfoRepository carInfoRepository;

    public CarInfoController(CarInfoRepository carInfoRepository) {
        this.carInfoRepository = carInfoRepository;
    }

    @DeleteMapping("/{vin}")
    public ResponseEntity<?> deleteById(@PathVariable String vin){
        carInfoRepository.deleteById(vin);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{carId}/{pickupDate}/{returnDate}")
    public Collection<CarInfo> findAvailableCarsForReservation(
            @PathVariable("carId") Long carId,
            @PathVariable("pickupDate") Date pickupDate,
            @PathVariable("returnDate") Date returnDate){
        Timestamp pickupTimestamp = new Timestamp(pickupDate.getTime());
        Timestamp returnTimestamp = new Timestamp(returnDate.getTime());
        return carInfoRepository.findAvailableCarsForReservation(carId, pickupTimestamp, returnTimestamp);
    }

    @PutMapping
    public ResponseEntity<CarInfo> updateCarInfo(@Valid @RequestBody CarInfo carInfo){
        Optional<CarInfo> foundCarInfo = carInfoRepository.findById(carInfo.getVin());
        if(foundCarInfo.isPresent()){
            CarInfo newCarInfo = foundCarInfo.get();
            newCarInfo.setAvailable(carInfo.isAvailable());
            newCarInfo.setOdometer(carInfo.getOdometer());
            carInfoRepository.save(newCarInfo);
            return ResponseEntity.ok().body(newCarInfo);
        }
        return ResponseEntity.notFound().build();
    }
}
