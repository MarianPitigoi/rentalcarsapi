package com.rentalcarsapi.controllers.DTO;

public class CarInfoRequest {
    private Long carId;
    private String vin;
    private boolean available;
    private String numberPlate;
    private int odometer;

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    @Override
    public String toString() {
        return "CarInfoRequest{" +
                "carId=" + carId +
                ", vin='" + vin + '\'' +
                ", available=" + available +
                ", numberPlate='" + numberPlate + '\'' +
                ", odometer=" + odometer +
                '}';
    }
}
