package com.rentalcarsapi.controllers.DTO;

import java.util.Date;

public class CarRequest {
    Date pickupDate;
    int pickupHour;
    Date returnDate;
    int returnHour;

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    public int getPickupHour() {
        return pickupHour;
    }

    public void setPickupHour(int pickupHour) {
        this.pickupHour = pickupHour;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public int getReturnHour() {
        return returnHour;
    }

    public void setReturnHour(int returnHour) {
        this.returnHour = returnHour;
    }

    @Override
    public String toString() {
        return "RentalCarRequest{" +
                "pickupDate=" + pickupDate +
                ", pickupHour=" + pickupHour +
                ", returnDate=" + returnDate +
                ", returnHour=" + returnHour +
                '}';
    }
}
