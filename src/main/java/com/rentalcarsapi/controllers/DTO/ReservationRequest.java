package com.rentalcarsapi.controllers.DTO;


import com.rentalcarsapi.entities.Car;
import com.rentalcarsapi.entities.CarInfo;
import com.rentalcarsapi.entities.Reservation;
import com.rentalcarsapi.entities.User;

import java.sql.Timestamp;


public class ReservationRequest {
    private Long reservationId;
    private Timestamp reservationPickupDate;
    private Timestamp reservationReturnDate;
    private Double reservationTotalPrice;

    private Long userId;
    private String userFirstName;
    private String userMiddleName;
    private String userLastName;
    private String userEmail;
    private String userPhoneNumber;

    private Long carId;
    private String carBrand;
    private String carModel;
    private String carCategory;
    private String carTransmission;
    private int carNumberOfPassengers;
    private Double carPricePerHour;

    private String carInfoVin;
    private boolean carInfoAvailable;
    private String carInfoNumberPlate;
    private int carInfoOdometer;

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Timestamp getReservationPickupDate() {
        return reservationPickupDate;
    }

    public void setReservationPickupDate(Timestamp reservationPickupDate) {
        this.reservationPickupDate = reservationPickupDate;
    }

    public Timestamp getReservationReturnDate() {
        return reservationReturnDate;
    }

    public void setReservationReturnDate(Timestamp reservationReturnDate) {
        this.reservationReturnDate = reservationReturnDate;
    }

    public Double getReservationTotalPrice() {
        return reservationTotalPrice;
    }

    public void setReservationTotalPrice(Double reservationTotalPrice) {
        this.reservationTotalPrice = reservationTotalPrice;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserMiddleName() {
        return userMiddleName;
    }

    public void setUserMiddleName(String userMiddleName) {
        this.userMiddleName = userMiddleName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarCategory() {
        return carCategory;
    }

    public void setCarCategory(String carCategory) {
        this.carCategory = carCategory;
    }

    public String getCarTransmission() {
        return carTransmission;
    }

    public void setCarTransmission(String carTransmission) {
        this.carTransmission = carTransmission;
    }

    public int getCarNumberOfPassengers() {
        return carNumberOfPassengers;
    }

    public void setCarNumberOfPassengers(int carNumberOfPassengers) {
        this.carNumberOfPassengers = carNumberOfPassengers;
    }

    public Double getCarPricePerHour() {
        return carPricePerHour;
    }

    public void setCarPricePerHour(Double carPricePerHour) {
        this.carPricePerHour = carPricePerHour;
    }

    public String getCarInfoVin() {
        return carInfoVin;
    }

    public void setCarInfoVin(String carInfoVin) {
        this.carInfoVin = carInfoVin;
    }

    public boolean isCarInfoAvailable() {
        return carInfoAvailable;
    }

    public void setCarInfoAvailable(boolean carInfoAvailable) {
        this.carInfoAvailable = carInfoAvailable;
    }

    public String getCarInfoNumberPlate() {
        return carInfoNumberPlate;
    }

    public void setCarInfoNumberPlate(String carInfoNumberPlate) {
        this.carInfoNumberPlate = carInfoNumberPlate;
    }

    public int getCarInfoOdometer() {
        return carInfoOdometer;
    }

    public void setCarInfoOdometer(int carInfoOdometer) {
        this.carInfoOdometer = carInfoOdometer;
    }

    public void extractReservationRequestFromReservation(Reservation reservation){

        setReservationId(reservation.getId());
        setReservationPickupDate(reservation.getPickupDate());
        setReservationReturnDate(reservation.getReturnDate());
        setReservationTotalPrice(reservation.getTotalPrice());

        User user = reservation.getUser();
        if(user!= null) {
            setUserId(user.getId());
            setUserFirstName(user.getFirstName());
            setUserMiddleName(user.getMiddleName());
            setUserLastName(user.getLastName());
            setUserEmail(user.getEmail());
            setUserPhoneNumber(user.getPhoneNumber());
        }

        Car car = reservation.getCar();
        if(car != null) {
            setCarId(car.getId());
            setCarBrand(car.getBrand());
            setCarModel(car.getModel());
            setCarCategory(car.getCategory());
            setCarTransmission(car.getTransmission());
            setCarNumberOfPassengers(car.getNumberOfPassengers());
            setCarPricePerHour(car.getPricePerHour());
        }

        CarInfo carInfo = reservation.getCarInfo();
        if(carInfo != null) {
            setCarInfoVin(carInfo.getVin());
            setCarInfoAvailable(carInfo.isAvailable());
            setCarInfoNumberPlate(carInfo.getNumberPlate());
            setCarInfoOdometer(carInfo.getOdometer());
        }
    }


}
