package com.rentalcarsapi.controllers.DTO;

public class CarInfoReservationRequest {
    private Long reservationId;
    private String carInfoVin;
    private boolean carInfoAvailability;
    private String carInfoNumberPlate;
    private int carInfoOdometer;

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public String getCarInfoVin() {
        return carInfoVin;
    }

    public void setCarInfoVin(String carInfoVin) {
        this.carInfoVin = carInfoVin;
    }

    public boolean isCarInfoAvailability() {
        return carInfoAvailability;
    }

    public void setCarInfoAvailability(boolean carInfoAvailability) {
        this.carInfoAvailability = carInfoAvailability;
    }

    public String getCarInfoNumberPlate() {
        return carInfoNumberPlate;
    }

    public void setCarInfoNumberPlate(String carInfoNumberPlate) {
        this.carInfoNumberPlate = carInfoNumberPlate;
    }

    public int getCarInfoOdometer() {
        return carInfoOdometer;
    }

    public void setCarInfoOdometer(int carInfoOdometer) {
        this.carInfoOdometer = carInfoOdometer;
    }
}
