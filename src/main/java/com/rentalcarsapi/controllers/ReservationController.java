package com.rentalcarsapi.controllers;

import com.rentalcarsapi.controllers.DTO.CarInfoReservationRequest;
import com.rentalcarsapi.controllers.DTO.ReservationRequest;
import com.rentalcarsapi.entities.CarInfo;
import com.rentalcarsapi.entities.Reservation;
import com.rentalcarsapi.repos.ReservationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rentalcars/reservations")
@CrossOrigin
public class ReservationController {

    private ReservationRepository reservationRepository;

    public ReservationController(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }


    @GetMapping
    public Collection<ReservationRequest> findAll(){
        List<Reservation> reservations = reservationRepository.findAll();
        List<ReservationRequest> reservationsRequestList = new ArrayList<>();
        if (!reservations.isEmpty()){
            reservations.forEach(r -> {
                ReservationRequest reservationRequest = new ReservationRequest();
                reservationRequest.extractReservationRequestFromReservation(r);
                reservationsRequestList.add(reservationRequest);
                    }
                   );
        }
        Collection<ReservationRequest> collectionReservations = reservationsRequestList;
        return collectionReservations;
    }

    @GetMapping("/{reservationId}")
    public ResponseEntity<ReservationRequest> findReservationRequest(@PathVariable("reservationId") Long reservationId){
        Optional<Reservation> foundReservation = reservationRepository.findById(reservationId);
        if (foundReservation.isPresent()) {
            Reservation reservation = foundReservation.get();
            ReservationRequest reservationRequest = new ReservationRequest();
            reservationRequest.extractReservationRequestFromReservation(reservation);
            return ResponseEntity.ok().body(reservationRequest);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    public ResponseEntity<Reservation> updateReservationWithNewCarInfo(
            @Valid @RequestBody CarInfoReservationRequest carInfoReservationRequest){
        Optional<Reservation> foundReservation = reservationRepository.findById(carInfoReservationRequest.getReservationId());

        if(foundReservation.isPresent()){
            Reservation reservation = foundReservation.get();
            CarInfo carInfo = new CarInfo();
            carInfo.setVin(carInfoReservationRequest.getCarInfoVin());
            carInfo.setAvailable(carInfoReservationRequest.isCarInfoAvailability());
            carInfo.setNumberPlate(carInfoReservationRequest.getCarInfoNumberPlate());
            carInfo.setOdometer(carInfoReservationRequest.getCarInfoOdometer());
            reservation.setCarInfo(carInfo);
            reservationRepository.save(reservation);
            return  ResponseEntity.ok().body(reservation);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
