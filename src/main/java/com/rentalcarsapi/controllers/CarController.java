package com.rentalcarsapi.controllers;

import com.rentalcarsapi.controllers.DTO.CarInfoRequest;
import com.rentalcarsapi.entities.Car;
import com.rentalcarsapi.entities.CarInfo;
import com.rentalcarsapi.repos.CarRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping("/rentalcars")
@CrossOrigin
public class CarController {

    private CarRepository carRepository;

    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping
    public Collection<Car> findAll(){
        return carRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Optional<Car> car = carRepository.findById(id);
        return car.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/available/{pickupDate}/{returnDate}")
    public Collection<Car> findAvailableBetweenDates(@PathVariable("pickupDate") Date pickupDate,
                                                     @PathVariable("returnDate") Date returnDate){
        Timestamp pickupTimestamp = new Timestamp(pickupDate.getTime());
        Timestamp returnTimestamp = new Timestamp(returnDate.getTime());
        return carRepository.findAvailableBetweenTimestamps(pickupTimestamp, returnTimestamp);
    }

    @PostMapping
    public ResponseEntity<Car> save(@Valid @RequestBody Car car) throws URISyntaxException {
        if(carRepository.findByBrandAndModelAndCategoryAndTransmissionAndNumberOfPassengersAndPricePerHour(
                        car.getBrand(),
                        car.getModel(),
                        car.getCategory(),
                        car.getTransmission(),
                        car.getNumberOfPassengers(),
                        car.getPricePerHour()
                ) == null) {
            carRepository.save(car);
            return ResponseEntity.created(new URI("/rentalcars/" + car.getId())).body(car);
        } else {
            return ResponseEntity.ok().body(car);
        }
    }

    @PutMapping
    public ResponseEntity<Car> addCar(@Valid @RequestBody CarInfoRequest carRequest){
        Optional<Car> foundCar = carRepository.findById(carRequest.getCarId());
        if(foundCar.isPresent()){
            Car car = foundCar.get();
            System.out.println("\n\n" + car + "\n\n");
            CarInfo carInfo = new CarInfo();
            carInfo.setVin(carRequest.getVin());
            carInfo.setAvailable(carRequest.isAvailable());
            carInfo.setNumberPlate(carRequest.getNumberPlate());
            carInfo.setOdometer(carRequest.getOdometer());
            car.addCarInfo(carInfo);
            carRepository.save(car);
            return ResponseEntity.ok().body(car);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<?> deleteById(@PathVariable Long id){
        carRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
