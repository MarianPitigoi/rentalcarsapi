package com.rentalcarsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentalcarsapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RentalcarsapiApplication.class, args);
    }

}
