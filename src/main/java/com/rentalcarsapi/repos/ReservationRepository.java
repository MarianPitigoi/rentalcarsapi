package com.rentalcarsapi.repos;

import com.rentalcarsapi.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ReservationRepository extends JpaRepository<Reservation, Long> {

}
