package com.rentalcarsapi.repos;

import com.rentalcarsapi.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
