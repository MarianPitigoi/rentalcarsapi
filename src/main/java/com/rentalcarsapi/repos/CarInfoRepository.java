package com.rentalcarsapi.repos;

import com.rentalcarsapi.entities.CarInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Collection;

public interface CarInfoRepository extends JpaRepository<CarInfo, String> {

    @Query(value = "SELECT * \n" +
            "FROM car_info c\n" +
            "WHERE c.available = 1 AND c.car_id = :carId AND \n" +
            "\tc.vin NOT IN(\n" +
            "    SELECT r.car_info_vin\n" +
            "\tFROM reservation r\n" +
            "\tWHERE r.car_info_vin IS NOT NULL AND\n" +
            "\tr.pickup_date BETWEEN :pickupDate AND :returnDate AND\n" +
            "\tr.return_date BETWEEN :pickupDate AND :returnDate);",
            nativeQuery = true)
    Collection<CarInfo> findAvailableCarsForReservation(
           @Param("carId") long carId,
           @Param("pickupDate") Timestamp pickupDate,
           @Param("returnDate") Timestamp returnDate) ;
}
