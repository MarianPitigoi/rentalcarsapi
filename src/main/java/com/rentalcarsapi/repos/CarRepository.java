package com.rentalcarsapi.repos;

import com.rentalcarsapi.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Collection;

public interface CarRepository extends JpaRepository<Car, Long> {
    Car findByBrandAndModelAndCategoryAndTransmissionAndNumberOfPassengersAndPricePerHour(
            String brand, String model, String category, String transmission,
            int numberOfPassengers, Double pricePerHour
    );

    @Query(value = "SELECT *\n" +
            "FROM car\n" +
            "WHERE id IN (\n" +
            "\tSELECT r.id\n" +
            "\tFROM (\n" +
            "\t\tSELECT c.id, count(ci.car_id) AS count_ci\n" +
            "\t\tFROM car c \n" +
            "\t\tJOIN car_info ci\n" +
            "\t\tON c.id = ci.car_id AND ci.available = 1\n" +
            "\t\tGROUP BY c.id) AS ci\n" +
            "\tJOIN (\n" +
            "\t\tSELECT c.id, count(r.car_id) AS count_r\n" +
            "\t\tFROM car c\n" +
            "\t\tLEFT JOIN reservation r\n" +
            "\t\tON c.id = r.car_id AND (r.pickup_date BETWEEN :pickupDate AND :returnDate AND\n" +
            "\t\tr.return_date BETWEEN :pickupDate AND :returnDate)\n" +
            "\t\tGROUP BY c.id) AS r\n" +
            "\tON ci.id = r.id AND ci.count_ci > r.count_r);",
            nativeQuery = true)
    Collection<Car> findAvailableBetweenTimestamps(@Param("pickupDate") Timestamp pickupDate,
                                                   @Param("returnDate") Timestamp returnDate);



}
