package com.rentalcarsapi.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"car"})
public class CarInfo {
    @Id
//    @Column(length = 17)
    private String vin;
    private boolean available;
    private String numberPlate;
    private int odometer;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    private Car car;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Reservation> reservations;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarInfo carInfo = (CarInfo) o;
        return Objects.equals(vin, carInfo.vin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vin);
    }

    public void addReservation(Reservation reservation){
        if (reservations == null) {
            setReservations(new HashSet<>());
        }
        reservation.setCarInfo(this);
        reservations.add(reservation);
    }

    @Override
    public String toString() {
        return "CarInfo{" +
                "vin='" + vin + '\'' +
                ", available=" + available +
                ", numberPlate='" + numberPlate + '\'' +
                ", odometer=" + odometer +
                ", car=" + car +
                ", reservations=" + reservations +
                '}';
    }
}
